Xonotic-Fun-Scripts
===================

Some scripts to make fun stuff with the [Xonotic](http://xonotic.org) console.

The latest version can be found at https://gitlab.com/mattia.basaglia/Xonotic-Fun-Scripts


Installation
------------

Copy the cfg files to your data directory (~/.xonotic/data or whatever it is in your system) .
Create autoexec.cfg (in the same directory) and add in it

    exec funlib.cfg


To bind a chat command to a key create a new user bind and set the command when pressed to something like this:

    commandmode mb_say ff0 000



Fancy Chats
-----------

Here are some commads to make colorful/funny in-game chats.
Each of these commands has three variants:
* ..._say       which sends the output in chat
* ..._echo      which prints the output in the console
* ..._print     which does both
* ..._say_team  which sends the output in team chat
* ..._name      which sets the player name 

### Rainbow color ###
"mb_rainbow_say" will print the text in saturated rainbow colors.
It only takes the string as argument.

### Blend two colors ###
"mb_say" will print the text where characters are colored with so that 
the first character is of the first color, the last character of the second color
and all other characters have a gradual blend between the two
 
It takes 3 parameters: the 12-bit rgb stings for the two colors ; and the string

example:

    mb_say ff0 000 Hello World
    
Prints Hello world from yellow to black.


### Arbitrary Gradient ###
"mb_say_n" will print the text with a gradient of any number of colors.

It takes a variable number of parameters: the first is a number that specifies
the number of colors, then the color values and the rest is used as input string.

example:

    mb_say_n 3 0f0 fff f00 Italian pizza
    
Prints the text in a green-white-red gradient.


### Random Emoticons ###

"mb_emoticon_say" will print a randomly-generated emoticon.

It takes no argument

Example input:

    mb_emoticon_say
    
Example output:

    :-)


Known Issues
------------

Only a limited set of character are supported:

* Printable ASCII (Basic Latin)
* Cyrillic
* Special Xonotic characters (those available in the player setup)
* A few extra 

If the input already contains color codes, the character following the color won't be recognised.
